# NERSC Technical Documentation

!!! tip "NERSC welcomes your contributions"
	These pages are hosted from a
	[git repository](https://gitlab.com/NERSC/nersc.gitlab.io) and
	[contributions](https://gitlab.com/NERSC/nersc.gitlab.io/blob/main/CONTRIBUTING.md)
	are welcome!

	[Fork this repo](https://gitlab.com/NERSC/nersc.gitlab.io/-/forks/new)

## What is NERSC?

[NERSC](https://www.nersc.gov) provides High Performance Computing and
Storage facilities and support for research sponsored by, and of
interest to, the U.S. Department of Energy (DOE) Office of Science
(SC). NERSC has the unique programmatic role of supporting all six
Office of Science program offices: 

  - Advanced Scientific Computing Research
  - Basic Energy Sciences
  - Biological and Environmental Research
  - Fusion Energy Sciences
  - High Energy Physics
  - Nuclear Physics.

Scientists who have been awarded research funding by any of the
offices are eligible to apply for an allocation of NERSC
time. Additional awards may be given to non-DOE funded project teams
whose research is aligned with the Office of Science's
mission. Allocations of time and storage are made by DOE.

NERSC is a national center, organizationally part of [Lawrence
Berkeley National Laboratory](https://www.lbl.gov) in Berkeley, CA.
NERSC staff and facilities are primarily located at Berkeley Lab's
Shyh Wang Hall on the Berkeley Lab campus.

## Computing Resources

### Perlmutter

Perlmutter is a [HPE Cray
EX](https://support.hpe.com/hpesc/public/docDisplay?docId=a00109703en_us&docLocale=en_US)
supercomputer that is currently being built. Its phase 1 system has
over 1500 GPU-accelerated compute nodes. The system contains CPU and GPU accelerated
nodes with [AMD EPYC 7763 (Milan)](https://www.amd.com/en/products/cpu/amd-epyc-7763) processor
with [NVIDIA A100](https://www.nvidia.com/en-us/data-center/a100/) GPUs

For more details see [Perlmutter System Specifications.](./systems/perlmutter/index.md)

### Cori

Cori is a [Cray XC40](https://www.hpe.com/us/en/what-is/cray-xc-series.html)
supercomputer with approximately 12000 compute nodes with two system partitions 
for **Haswell** and **KNL** nodes. The Haswell nodes are running 
[Intel Xeon Processor E5-2698 v3](https://ark.intel.com/products/81060/Intel-Xeon-Processor-E5-2698-v3-40M-Cache-2_30-GHz)
with 32Cores/Node with 2 Threads/Core for total of 64 VCPUs/node. The KNL nodes are 
running [Intel Xeon Phi Processor 7250](http://ark.intel.com/products/94035/Intel-Xeon-Phi-Processor-7250-16GB-1_40-GHz-68-core)
that consist of 68Cores/Node with 4 Threads/Core for total of 272 VCPUs/Node.

Cori comes with a Large Memory partition with 20 nodes using 
[AMD EPYC 7302 (Rome)](https://www.amd.com/en/products/cpu/amd-epyc-7302) each with 32 Cores/Node and
2 Threads/Core for total of 64 Cores/Node.

For more details see [Cori system Specifications.](./systems/cori/index.md)

## NERSC Users Group (NUG)

Join the [NERSC Users Group](https://www.nersc.gov/users/NUG/): an
independent organization of users of NERSC resources.

!!! tip
    NUG maintains a Slack workspace that all users are welcome to
    [join](https://www.nersc.gov/users/NUG/nersc-users-slack/).
    
## References

* [NERSC Home page](https://nersc.gov) - center news and information
* [MyNERSC](https://my.nersc.gov) - System Status, and interactive access to NERSC resources
* [iris](https://iris.nersc.gov) - For account management
* [Help Portal](https://help.nersc.gov) - open tickets, make requests
* [Getting Started](./getting-started.md) - overview of NERSC documentation for new and existing users. 
