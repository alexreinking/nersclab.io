# LAPACK

Linear Algebra PACKage (LAPACK) provides routines for solving systems
of simultaneous linear equations, least-squares solutions of linear
systems of equations, eigenvalue problems, and singular value
problems.

## Optimized versions

* [MKL](../mkl/index.md)
* [Cray LibSci](../libsci/index.md)
